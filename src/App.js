import React from 'react';
import logo from './logo.svg';
import './App.css';

import publisher from './services/publisher';

function App() {
  const clickHandler = () => {
    const title = document.getElementById('title').value;
    const content = document.getElementById('content').value;

    publisher.post(content, title);
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
      <main>
        <form>
          <label htmlFor="title">
            Title
            <input type="text" id="title" name="title" />
          </label>
          <label htmlFor="content">
            Content
            <textarea id="content" name="content" />
          </label>
          <button type="button" onClick={clickHandler}>Post</button>
        </form>
      </main>
    </div>
  );
}

export default App;
