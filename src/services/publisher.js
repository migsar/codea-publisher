class Publisher {
  post(content, title) {
    fetch('http://0.0.0.0:3000', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ title, content })
    });
  }
}

export default new Publisher();
