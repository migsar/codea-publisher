FROM node:10-alpine as builder

COPY package.json yarn.lock ./

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force

# Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN yarn install && mkdir /codea-bot-simple-cli && cp -R ./node_modules ./codea-bot-simple-cli

WORKDIR /codea-bot-simple-cli

COPY . .

RUN yarn build


FROM nginx:1.13.3-alpine

# Copy our custom nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

# Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# Copy build/ directory from 'builder' stage
COPY --from=builder /codea-bot-simple-cli/build /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
